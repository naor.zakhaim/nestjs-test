import { Delete, Injectable, NotFoundException } from '@nestjs/common';
import { Product } from './product.model';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class ProductsService {
  private products: Product[] = [];

  insertProduct(title: string, desc: string, price: number): Product {
    const id: string = uuidv4();
    const newProduct = new Product(id, title, desc, price);
    this.products.push(newProduct);

    return newProduct;
  }

  getProducts(): Product[] {
    return [...this.products]; // we don't want to return the exact pointer of the array, in which case the client could change the original array
  }

  getSingleProduct(productId: string): Product {
    const product = this.findProduct(productId)[0];
    return { ...product };
  }

  updateProduct(productId: string, title: string, desc: string, price: number) {
    const [product, index] = this.findProduct(productId);
    const updatedProduct = { ...product };
    if (title) {
      updatedProduct.title = title;
    }
    if (desc) {
      updatedProduct.description = desc;
    }
    if (price) {
      updatedProduct.price = price;
    }

    this.products[index] = updatedProduct;

    return this.products[index];
  }

  deleteProduct(productId: string) {
    const [_, index] = this.findProduct(productId);
    console.log(this.products[index]);

    this.products.splice(index, 1);
  }

  private findProduct(id: string): [Product, number] {
    const productIndex = this.products.findIndex((prod) => prod.id === id);
    const product = this.products[productIndex];

    if (!product) {
      throw new NotFoundException('Could not found product');
    }
    return [product, productIndex];
  }
}
