import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { Product } from './product.model';
import { ProductsService } from './products.service';

@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Post()
  addProduct(
    @Body()
    completeBody: {
      title: string;
      description: string;
      price: number;
    },
  ): Product {
    const { title, description, price } = completeBody;
    return this.productsService.insertProduct(title, description, price);
  }

  @Post('different')
  addProductWithDifferentBodyAccess(
    @Body('title') ProdTitle: string,
    @Body('description') ProdDescription: string,
    @Body('price') ProdPrice: number,
  ) {
    return this.productsService.insertProduct(
      ProdTitle,
      ProdDescription,
      ProdPrice,
    );
  }

  @Get()
  getAllProducts() {
    return this.productsService.getProducts();
  }

  @Get(':id')
  getProduct(@Param('id') prodId: string) {
    return this.productsService.getSingleProduct(prodId);
  }

  @Patch(':id')
  UpdateProduct(
    @Param('id') prodId: string,
    @Body('title') prodTitle: string,
    @Body('description') prodDesc: string,
    @Body('price') prodPrice: number,
  ) {
    return this.productsService.updateProduct(
      prodId,
      prodTitle,
      prodDesc,
      prodPrice,
    );
  }

  @Delete(':id')
  removeProduct(@Param('id') prodId: string) {
    this.productsService.deleteProduct(prodId);
    return null;
  }
}
